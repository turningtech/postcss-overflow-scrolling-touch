let postcss = require("postcss");

module.exports = postcss.plugin("postcss-overflow-scrolling-touch", function() {
    return function(root) {
        root.walkRules(function(rule) {
            rule.walkDecls(/^overflow-?/, function(decl) {
                if (["scroll", "auto", "inherit"].includes(decl.value)) {
                    let hasTouch = rule.some(function(i) {
                        return i.prop === "-webkit-overflow-scrolling";
                    });
                    if (!hasTouch) {
                        rule.append({
                            prop: "-webkit-overflow-scrolling",
                            value: "touch",
                        });
                    }
                }
            });
        });
    };
});
