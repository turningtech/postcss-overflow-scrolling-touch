let postcss = require("postcss");

let plugin = require("./");

function run(input, output, opts) {
    return postcss([plugin(opts)])
        .process(input)
        .then(function(result) {
            expect(result.css).toEqual(output);
            expect(result.warnings()).toHaveLength(0);
        });
}

it("Adds `-webkit-overflow-scrolling:touch`", function() {
    return run("a{overflow-y:auto;}", "a{overflow-y:auto;-webkit-overflow-scrolling:touch;}", {});
});

it("Does not add `-webkit-overflow-scrolling:touch`", function() {
    return run("a{overflow-y:hidden;}", "a{overflow-y:hidden;}", {});
});
